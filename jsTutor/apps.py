from django.apps import AppConfig


class JstutorConfig(AppConfig):
    name = 'jsTutor'
