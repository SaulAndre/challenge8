from django.urls import path
from .views import index, testAjax

urlpatterns = [
    path('', index),
    path('test', testAjax),
]