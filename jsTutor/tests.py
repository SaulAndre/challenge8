from django.test import TestCase

# Create your tests here.
from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time
# Create your tests here.

class JSTest(TestCase):
    def test_request_web2(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)
    
    def test_web2_using_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'home.html')
    
    def test_func_web2(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

class Web2FunctionalTest(TestCase):

    def setUp(self):
        chrome_options = Options()
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(Web2FunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(Web2FunctionalTest, self).tearDown()

    def test_input_status(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get('http://127.0.0.1:8000/')
        time.sleep(5)
        # find the form element
        status = selenium.find_element_by_id('id_status')
        button = selenium.find_element_by_name('send')

        # Fill the form with data
        status.send_keys('baik baik saja')
        button.send_keys(Keys.RETURN)

        time.sleep(20)
        # submitting the form
        webdriver.Chrome().quit()

    
